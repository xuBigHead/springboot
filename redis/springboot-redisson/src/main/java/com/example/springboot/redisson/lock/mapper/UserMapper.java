package com.example.springboot.redisson.lock.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.common.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author xmm
 * @since 2020/1/16
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
