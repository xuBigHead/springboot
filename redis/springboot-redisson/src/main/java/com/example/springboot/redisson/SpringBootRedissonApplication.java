package com.example.springboot.redisson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author xmm
 */
@SpringBootApplication
@EnableConfigurationProperties
public class SpringBootRedissonApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootRedissonApplication.class, args);
    }
}
