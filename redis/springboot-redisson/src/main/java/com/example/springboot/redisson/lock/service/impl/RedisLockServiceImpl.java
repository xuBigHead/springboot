package com.example.springboot.redisson.lock.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.entity.User;
import com.example.common.result.R;
import com.example.springboot.redisson.lock.mapper.UserMapper;
import com.example.springboot.redisson.lock.service.RedisLockService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author xmm
 * @since 2020/3/24
 */
@Slf4j
@Service
@AllArgsConstructor
public class RedisLockServiceImpl extends ServiceImpl<UserMapper, User> implements RedisLockService {
    Redisson redisson;

    @Override
    public R updateUserAge(User user) {
        User userFromDb = this.getById(user.getId());
        user.setAge(userFromDb.getAge() + 1);
        user.setUpdateTime(new Date());
        return R.status(this.updateById(user));
    }

    @Override
    public R updateUserAgeWithLock(User user) {
        RLock lock = redisson.getLock("1");
        lock.lock(10L, TimeUnit.SECONDS);
        User userFromDb;
        try {
            userFromDb = this.getById(user.getId());
            user.setAge(userFromDb.getAge() + 1);
            user.setUpdateTime(new Date());
            return R.status(this.updateById(user));
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("update user age failed");
        } finally {
            lock.unlock();
        }
    }
}
