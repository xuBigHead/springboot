package com.example.springboot.redisson.lock.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.entity.User;
import com.example.common.result.R;

/**
 * @author xmm
 * @since 2020/3/24
 */
public interface RedisLockService extends IService<User> {
    /**
     * update user age without redis lock
     *
     * @param user user
     * @return R
     */
    R updateUserAge(User user);

    /**
     * update user age without redis lock
     *
     * @param user user
     * @return R
     */
    R updateUserAgeWithLock(User user);
}
