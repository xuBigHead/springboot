package com.example.springboot.redisson.lock.controller;

import com.example.common.entity.User;
import com.example.common.result.R;
import com.example.springboot.redisson.lock.service.RedisLockService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xmm
 * @since 2020/3/24
 */
@RestController
@RequestMapping("/redis/lock")
@AllArgsConstructor
public class RedisLockController {
    RedisLockService redisLockService;

    @GetMapping("/update")
    public R update(){
        User user = new User();
        user.setId(1204710899061199140L);
        return redisLockService.updateUserAge(user);
    }

    @GetMapping("/update/lock")
    public R updateWithLock(){
        User user = new User();
        user.setId(1204710899061199140L);
        return redisLockService.updateUserAgeWithLock(user);
    }
}
