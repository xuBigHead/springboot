package com.example.jedis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author xmm
 */
@SpringBootApplication
@EnableConfigurationProperties
public class JedisApplication {
    public static void main(String[] args) {
        SpringApplication.run(JedisApplication.class, args);
    }
}
