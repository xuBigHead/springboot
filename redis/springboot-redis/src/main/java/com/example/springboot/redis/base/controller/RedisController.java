package com.example.springboot.redis.base.controller;

import com.example.common.result.R;
import com.example.common.utils.ControllerUtil;
import com.example.springboot.redis.base.service.RedisService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@RestController
@RequestMapping("/redis/api")
@AllArgsConstructor
public class RedisController {
    private RedisService redisService;
    @GetMapping("/{method}")
    public R api(@PathVariable("method") String method){
        return ControllerUtil.<RedisService>build().run(redisService, method);
    }
}
