package com.example.springboot.redis.base.controller;

import com.example.springboot.redis.base.service.ListRedisService;
import com.example.common.result.R;
import com.example.common.utils.ControllerUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@RestController
@RequestMapping("/redis/list/api")
@AllArgsConstructor
public class RedisListController {
    private ListRedisService listRedisService;
    @GetMapping("/{method}")
    public R api(@PathVariable("method") String method){
        return ControllerUtil.<ListRedisService>build().run(listRedisService, method);
    }
}
