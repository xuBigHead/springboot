package com.example.springboot.redis.base.controller;

import com.example.springboot.redis.base.service.StringRedisService;
import com.example.common.result.R;
import com.example.common.utils.ControllerUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@RestController
@RequestMapping("/redis/string/api")
@AllArgsConstructor
public class RedisStringController {
    private StringRedisService stringRedisService;
    @GetMapping("/{method}")
    public R api(@PathVariable("method") String method){
        return ControllerUtil.<StringRedisService>build().run(stringRedisService, method);
    }
}
