package com.example.springboot.redis.base.service.impl;

import com.example.springboot.redis.base.consts.key.RedisListKeyConst;
import com.example.springboot.redis.base.service.RedisService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;


/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@Service
@AllArgsConstructor
public class RedisServiceImpl implements RedisService {
    private StringRedisTemplate stringRedisTemplate;
    private RedisTemplate redisTemplate;
    @Override
    public void getConnectionFactory(){
        log.info("redis连接工厂：{}",redisTemplate.getConnectionFactory());
    }

    @Override
    public void opsForDataType() {
        stringRedisTemplate.opsForValue().set("any-key:string", "string:value");
        stringRedisTemplate.opsForList().leftPush("any-key:list", "list:value");
        stringRedisTemplate.opsForHash().put("any-key:hash", "hash", "hash:value");
        stringRedisTemplate.opsForSet().add("any-key:set", "set:value");
        stringRedisTemplate.opsForZSet().add("any-key:zset","zset:value", 1);
    }

    @Override
    public void boundDataTypeOps(){
        stringRedisTemplate.boundValueOps(RedisListKeyConst.STRING).set("string:value");
        stringRedisTemplate.boundListOps(RedisListKeyConst.LIST).set(1L, "list:value");
        stringRedisTemplate.boundHashOps(RedisListKeyConst.HASH).put("hash", "hash:value");
        stringRedisTemplate.boundSetOps(RedisListKeyConst.SET).add("set:value");
        stringRedisTemplate.boundZSetOps(RedisListKeyConst.ZSET).add("zset:value", 1);
    }
}
