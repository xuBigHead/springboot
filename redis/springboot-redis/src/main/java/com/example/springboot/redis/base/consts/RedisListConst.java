package com.example.springboot.redis.base.consts;

/**
 * @author xmm
 * @since 2020/1/11
 */
public interface RedisListConst {
    String REDIS = "/redis";
    String LIST = "/list";
    String LIST_PUSH = LIST + "/push";
    String LIST_INDEX = LIST + "/index";
    String LIST_SIZE = LIST + "/size";
}
