package com.example.springboot.redis.base.service;

/**
 * @author xmm
 * @since 2020/1/11
 */
public interface RedisService {
    /**
     * 获取当前redis客户端工厂类
     * @return R
     */
    void getConnectionFactory();

    /**
     * stringRedisTemplate操作5中数据结构，没有指定key，可以对多个key进行存取
     */
    void opsForDataType();

    /**
     * stringRedisTemplate操作5中数据结构，只能对指定的key进行存取
     */
    void boundDataTypeOps();
}
