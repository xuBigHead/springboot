package com.example.springboot.redis.base.service;

import com.example.common.result.R;

/**
 * @author xmm
 * @since 2020/1/11
 */
public interface ListRedisService {
    void push(Integer ops);

    R index();

    R size();
}
