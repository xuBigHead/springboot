package com.example.springboot.redis.base.service.impl;

import com.example.common.entity.User;
import com.example.springboot.redis.base.service.HashRedisService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@Service
@AllArgsConstructor
public class HashRedisServiceImpl implements HashRedisService {
    StringRedisTemplate stringRedisTemplate;

    public void values(){
        HashOperations<String, String, User> hashOperations = stringRedisTemplate.opsForHash();
        List<User> key = hashOperations.values("key");
    }
}
