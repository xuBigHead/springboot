package com.example.springboot.redis.base.consts.key;
/**
 * @author xmm
 * @since 2020/1/11
 */
public interface RedisListKeyConst {
    String LIST = "list";
    String STRING = "string";
    String HASH = "hash";
    String SET = "set";
    String ZSET = "zset";
}
