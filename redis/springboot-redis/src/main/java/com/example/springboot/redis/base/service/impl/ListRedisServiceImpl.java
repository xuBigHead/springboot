package com.example.springboot.redis.base.service.impl;

import com.alibaba.fastjson.JSON;
import com.example.springboot.redis.base.consts.key.RedisListKeyConst;
import com.example.springboot.redis.base.service.ListRedisService;
import com.example.common.entity.collection.NumList;
import com.example.common.result.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@Service
@AllArgsConstructor
public class ListRedisServiceImpl implements ListRedisService {
    StringRedisTemplate stringRedisTemplate;
    RedisTemplate redisTemplate;
    @Override
    public void push(Integer ops){
        List<Integer> numList = new NumList().getNumList();
        if(1 == ops) {
            for (Integer integer : numList) {
                String jsonString = JSON.toJSONString(integer);
                stringRedisTemplate.opsForList().leftPush(RedisListKeyConst.LIST, jsonString);
            }
        } else {
            for (Integer integer : numList) {
                String jsonString = JSON.toJSONString(integer);
                stringRedisTemplate.opsForList().rightPush(RedisListKeyConst.LIST, jsonString);
            }
        }

    }

    @Override
    public R index(){
        List<Integer> numList = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            String jsonString = stringRedisTemplate.opsForList().index(RedisListKeyConst.LIST, i);
            Integer num = JSON.parseObject(jsonString, Integer.class);
            numList.add(num);
            log.info("索引[{}]的值为[{}]",i,num);
        }
        return R.data(numList);
    }

    @Override
    public R size() {
        Long size = stringRedisTemplate.opsForList().size(RedisListKeyConst.LIST);
        log.info("[{}]集合的长度为[{}]",RedisListKeyConst.LIST,size);
        return R.data(size);
    }
}
