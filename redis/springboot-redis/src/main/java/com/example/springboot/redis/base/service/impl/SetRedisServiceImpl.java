package com.example.springboot.redis.base.service.impl;

import com.example.springboot.redis.base.service.SetRedisService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Slf4j
@Service
@AllArgsConstructor
public class SetRedisServiceImpl implements SetRedisService {
}
