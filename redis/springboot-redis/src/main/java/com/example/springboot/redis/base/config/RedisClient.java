package com.example.springboot.redis.base.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.data.redis.core.*;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author mangmang.xu
 * @since 2020/4/15
 */
@Getter
@RequiredArgsConstructor
@SuppressWarnings("unchecked")
public class RedisClient<T> implements SmartInitializingSingleton {
    private final RedisTemplate<String, T> redisTemplate;
    private ValueOperations<String, T> valueOps;
    private HashOperations<String, Object, T> hashOps;
    private ListOperations<String, T> listOps;
    private SetOperations<String, T> setOps;
    private ZSetOperations<String, T> zSetOps;


    public void set(CacheKey cacheKey, T value) {
        String key = cacheKey.getKey();
        Duration expire = cacheKey.getExpire();
        if (expire == null) {
            set(key, value);
        } else {
            setEx(key, value, expire);
        }
    }


    public void set(String key, T value) {
        valueOps.set(key, value);
    }


    public void setEx(String key, T value, Duration timeout) {
        valueOps.set(key, value, timeout);
    }


    public void setEx(String key, T value, Long seconds) {
        valueOps.set(key, value, seconds, TimeUnit.SECONDS);
    }


    @Nullable
    public <T> T get(String key) {
        return (T) valueOps.get(key);
    }


    //@Nullable
    //public <T> T get(String key, Supplier<T> loader) {
    //    T value = this.get(key);
    //    if (value != null) {
    //        return value;
    //    }
    //    value = loader.get();
    //    if (value == null) {
    //        return null;
    //    }
    //    this.set(key, value);
    //    return value;
    //}


    @Nullable
    public <T> T get(CacheKey cacheKey) {
        return (T) valueOps.get(cacheKey.getKey());
    }


    //@Nullable
    //public <T> T get(CacheKey cacheKey, Supplier<T> loader) {
    //    String key = cacheKey.getKey();
    //    T value = this.get(key);
    //    if (value != null) {
    //        return value;
    //    }
    //    value = loader.get();
    //    if (value == null) {
    //        return null;
    //    }
    //    this.set(cacheKey, value);
    //    return value;
    //}


    public Boolean del(String key) {
        return redisTemplate.delete(key);
    }


    public Boolean del(CacheKey key) {
        return redisTemplate.delete(key.getKey());
    }


    public Long del(String... keys) {
        return del(Arrays.asList(keys));
    }


    public Long del(Collection<String> keys) {
        return redisTemplate.delete(keys);
    }


    public Set<String> keys(String pattern) {
        return redisTemplate.keys(pattern);
    }


    //public void mSet(Object... keysValues) {
    //    valueOps.multiSet(CollectionUtil.toMap(keysValues));
    //}


    public List<T> mGet(String... keys) {
        return mGet(Arrays.asList(keys));
    }


    public List<T> mGet(Collection<String> keys) {
        return valueOps.multiGet(keys);
    }


    public Long decr(String key) {
        return valueOps.decrement(key);
    }


    public Long decrBy(String key, long longValue) {
        return valueOps.decrement(key, longValue);
    }


    public Long incr(String key) {
        return valueOps.increment(key);
    }


    public Long incrBy(String key, long longValue) {
        return valueOps.increment(key, longValue);
    }


    public Long getCounter(String key) {
        return Long.valueOf(String.valueOf(valueOps.get(key)));
    }


    public Boolean exists(String key) {
        return redisTemplate.hasKey(key);
    }


    public String randomKey() {
        return redisTemplate.randomKey();
    }


    public void rename(String oldkey, String newkey) {
        redisTemplate.rename(oldkey, newkey);
    }


    public Boolean move(String key, int dbIndex) {
        return redisTemplate.move(key, dbIndex);
    }


    public Boolean expire(String key, long seconds) {
        return redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
    }


    public Boolean expire(String key, Duration timeout) {
        return expire(key, timeout.getSeconds());
    }


    public Boolean expireAt(String key, Date date) {
        return redisTemplate.expireAt(key, date);
    }


    public Boolean expireAt(String key, long unixTime) {
        return expireAt(key, new Date(unixTime));
    }


    public Boolean pexpire(String key, long milliseconds) {
        return redisTemplate.expire(key, milliseconds, TimeUnit.MILLISECONDS);
    }


    //public <T> T getSet(String key, T value) {
    //    return (T) valueOps.getAndSet(key, value);
    //}


    public Boolean persist(String key) {
        return redisTemplate.persist(key);
    }


    public String type(String key) {
        return redisTemplate.type(key).code();
    }


    public Long ttl(String key) {
        return redisTemplate.getExpire(key);
    }


    public Long pttl(String key) {
        return redisTemplate.getExpire(key, TimeUnit.MILLISECONDS);
    }


    public void hSet(String key, Object field, T value) {
        hashOps.put(key, field, value);
    }


    public void hMset(String key, Map<Object, T> hash) {
        hashOps.putAll(key, hash);
    }


    public <T> T hGet(String key, Object field) {
        return (T) hashOps.get(key, field);
    }


    public List hmGet(String key, Object... fields) {
        return hmGet(key, Arrays.asList(fields));
    }


    public List hmGet(String key, Collection<Object> hashKeys) {
        return hashOps.multiGet(key, hashKeys);
    }


    public Long hDel(String key, Object... fields) {
        return hashOps.delete(key, fields);
    }


    public Boolean hExists(String key, Object field) {
        return hashOps.hasKey(key, field);
    }


    public Map hGetAll(String key) {
        return hashOps.entries(key);
    }


    public List hVals(String key) {
        return hashOps.values(key);
    }


    public Set<Object> hKeys(String key) {
        return hashOps.keys(key);
    }


    public Long hLen(String key) {
        return hashOps.size(key);
    }


    public Long hIncrBy(String key, Object field, long value) {
        return hashOps.increment(key, field, value);
    }


    public Double hIncrByFloat(String key, Object field, double value) {
        return hashOps.increment(key, field, value);
    }


    public <T> T lIndex(String key, long index) {
        return (T) listOps.index(key, index);
    }


    public Long lLen(String key) {
        return listOps.size(key);
    }


    public <T> T lPop(String key) {
        return (T) listOps.leftPop(key);
    }


    public Long lPush(String key, T values) {
        return listOps.leftPush(key, values);
    }


    public void lSet(String key, long index, T value) {
        listOps.set(key, index, value);
    }


    public Long lRem(String key, long count, T value) {
        return listOps.remove(key, count, value);
    }


    public List lRange(String key, long start, long end) {
        return listOps.range(key, start, end);
    }


    public void lTrim(String key, long start, long end) {
        listOps.trim(key, start, end);
    }


    public <T> T rPop(String key) {
        return (T) listOps.rightPop(key);
    }


    public Long rPush(String key, T values) {
        return listOps.rightPush(key, values);
    }


    public <T> T rPopLPush(String srcKey, String dstKey) {
        return (T) listOps.rightPopAndLeftPush(srcKey, dstKey);
    }


    public Long sAdd(String key, T... members) {
        return setOps.add(key, members);
    }


    public <T> T sPop(String key) {
        return (T) setOps.pop(key);
    }


    public Set sMembers(String key) {
        return setOps.members(key);
    }


    public boolean sIsMember(String key, Object member) {
        return setOps.isMember(key, member);
    }


    public Set sInter(String key, String otherKey) {
        return setOps.intersect(key, otherKey);
    }


    public Set sInter(String key, Collection<String> otherKeys) {
        return setOps.intersect(key, otherKeys);
    }


    public <T> T sRandMember(String key) {
        return (T) setOps.randomMember(key);
    }


    public List sRandMember(String key, int count) {
        return setOps.randomMembers(key, count);
    }


    public Long sRem(String key, Object... members) {
        return setOps.remove(key, members);
    }


    public Set sUnion(String key, String otherKey) {
        return setOps.union(key, otherKey);
    }


    public Set sUnion(String key, Collection<String> otherKeys) {
        return setOps.union(key, otherKeys);
    }


    public Set sDiff(String key, String otherKey) {
        return setOps.difference(key, otherKey);
    }


    public Set sDiff(String key, Collection<String> otherKeys) {
        return setOps.difference(key, otherKeys);
    }


    public Boolean zAdd(String key, T member, double score) {
        return zSetOps.add(key, member, score);
    }


    public Long zAdd(String key, Map<T, Double> scoreMembers) {
        Set<ZSetOperations.TypedTuple<T>> tuples = new HashSet<>();
        scoreMembers.forEach((k, v) -> {
            tuples.add(new DefaultTypedTuple<>(k, v));
        });
        return zSetOps.add(key, tuples);
    }


    public Long zCard(String key) {
        return zSetOps.zCard(key);
    }


    public Long zCount(String key, double min, double max) {
        return zSetOps.count(key, min, max);
    }


    public Double zIncrBy(String key, T member, double score) {
        return zSetOps.incrementScore(key, member, score);
    }


    public Set zRange(String key, long start, long end) {
        return zSetOps.range(key, start, end);
    }


    public Set zRevrange(String key, long start, long end) {
        return zSetOps.reverseRange(key, start, end);
    }


    public Set zRangeByScore(String key, double min, double max) {
        return zSetOps.rangeByScore(key, min, max);
    }


    public Long zRank(String key, T member) {
        return zSetOps.rank(key, member);
    }


    public Long zRevrank(String key, T member) {
        return zSetOps.reverseRank(key, member);
    }


    public Long zRem(String key, T... members) {
        return zSetOps.remove(key, members);
    }


    public Double zScore(String key, T member) {
        return zSetOps.score(key, member);
    }

    @Override
    public void afterSingletonsInstantiated() {
        Assert.notNull(redisTemplate, "redisTemplate is null");
        valueOps = redisTemplate.opsForValue();
        hashOps = redisTemplate.opsForHash();
        listOps = redisTemplate.opsForList();
        setOps = redisTemplate.opsForSet();
        zSetOps = redisTemplate.opsForZSet();
    }
}

