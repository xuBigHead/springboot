package com.example.boot.base.config;

import com.example.boot.base.entity.diy.Yml;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author mangmang.xu
 * @since 2020/4/28
 */
@Slf4j
@Data
@PropertySource(value = "classpath:diy.yml")
@Configuration
public class YmlConfiguration {

    @Value("${com.example.spring.name}")
    String name;

    //@Value("com.example.spring.time")
    @Value("2020-04-28 18:00:00")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date date;

    @Bean
    public Yml getYml(){
        Yml yml = new Yml();
        yml.setName(name);
        yml.setTime(date);
        log.info("yml: {}", yml);
        return yml;
    }
}
