package com.example.boot.base.entity.diy;

import lombok.Data;

/**
 * @author mangmang.xu
 * @since 2020/4/28
 */
@Data
public class Properties {
    String name;
}
