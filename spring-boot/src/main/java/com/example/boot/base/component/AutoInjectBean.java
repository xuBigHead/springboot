package com.example.boot.base.component;

import com.example.boot.base.entity.User;
import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


/**
 * @author xmm
 * @since 2020/3/5
 */
@Data
@Component(value = "autoInject")
public class AutoInjectBean {
    String title = "autoInjectBean";
    
    @Bean(value = "defaultUser")
    public User getDefaultUser(){
        User user = new User();
        user.setName("许芒芒");
        return user;
    }
}
