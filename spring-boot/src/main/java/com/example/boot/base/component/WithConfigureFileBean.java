package com.example.boot.base.component;

import com.example.boot.base.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author xmm
 * @since 2020/1/17
 */
@Slf4j
@Component
@PropertySource(value= {"classpath:config/config_1.properties","classpath:config/config_2.properties"},
        ignoreResourceNotFound = true)
public class WithConfigureFileBean {
    @Value("${field_one}")
    private String fieldOne;

    @Value("${field_two}")
    private String count;

    @Bean(value = "userFromFile")
    public User getUser() {
        User user = new User();
        user.setName(fieldOne);
        user.setCount(new Long(count));
        log.info("user: {}", user);
        return user;
    }
}
