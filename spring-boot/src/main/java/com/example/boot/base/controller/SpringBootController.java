package com.example.boot.base.controller;

import com.example.boot.base.component.WithoutConfigureFileBean;
import com.example.boot.base.config.YmlConfiguration;
import com.example.common.result.R;
import com.example.boot.base.config.PropertiesConfiguration;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注解@Controller标明这是一个SpringMVC的Controller控制器
 * @author xmm
 * @since 2020/1/17
 */
@Slf4j
@RestController
@RequestMapping("/springboot")
@AllArgsConstructor
public class SpringBootController {
    private WithoutConfigureFileBean withoutConfigureFileBean;

    @GetMapping("/without/configure")
    public R withoutConfigure() {
        return R.data(withoutConfigureFileBean);
    }

    @GetMapping("/temp")
    public R temp() {
        return R.success("success");
    }

    @GetMapping("/temp2")
    public R temp2() {
        return R.success("success");
    }

    @GetMapping("/get-diy-yml")
    public R getDiyYml(){
        YmlConfiguration ymlConfiguration = new YmlConfiguration();
        log.info("diy.yml : {}", ymlConfiguration);
        PropertiesConfiguration propertiesConfiguration = new PropertiesConfiguration();
        log.info("diy.properties : {}", propertiesConfiguration);
        return R.success("success");
    }
}
