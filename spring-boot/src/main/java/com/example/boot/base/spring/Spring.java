package com.example.boot.base.spring;

import com.example.boot.base.component.AutoInjectBean;
import com.example.boot.base.config.BaseConfiguration;
import com.example.boot.base.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author xmm
 * @since 2020/3/5
 */
@Slf4j
public class Spring {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(BaseConfiguration.class);
        User defaultUser = context.getBean("defaultUser",User.class);
        System.err.println("default user's name : " + defaultUser.getName());

        AutoInjectBean autoInjectBean = context.getBean(AutoInjectBean.class);
        System.err.println("autoInjectBean's title is : " + autoInjectBean.getTitle());

        User userFromFile = context.getBean("userFromFile", User.class);
        System.err.println("user from file name is : " + userFromFile.getName());
        System.err.println("user from file count is : " + userFromFile.getCount());
    }
}
