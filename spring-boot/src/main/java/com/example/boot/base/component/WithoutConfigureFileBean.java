package com.example.boot.base.component;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * @author xmm
 * @since 2020/1/17
 */
@Data
@Component
public class WithoutConfigureFileBean {
    @Value("注入普通字符串")
    private String normal;

    //关于属性的KEY可以查看System类说明
    @Value("#{systemProperties['java.version']}")//-->使用了SpEL表达式
    private String systemPropertiesName;

    @Value("#{T(java.lang.Math).random()*80}")//获取随机数
    private double randomNumber;

    @Value("#{1+2}")
    private double sum;

    @Value("classpath:config/diy-configuration_1.yml")
    @JsonSerialize(using = ToStringSerializer.class)
    private Resource resourceFile;

    @Value("http://www.baidu.com")
    @JsonSerialize(using = ToStringSerializer.class)
    private Resource testUrl;

    @Value("#{defaultUser.name}")
    private String studentName;
}
