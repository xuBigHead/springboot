package com.example.boot.base.entity.animal;

import com.example.boot.base.entity.Animal;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author xmm
 * @since 2020/3/28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Dog extends Animal {
    String name;
    String master;
}
