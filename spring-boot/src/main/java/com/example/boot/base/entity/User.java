package com.example.boot.base.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author xmm
 * @since 2020/1/17
 */
@Data
public class User {
    private String name;
    private Long count;
    private Date createTime;
    private Date updateTime;
}
