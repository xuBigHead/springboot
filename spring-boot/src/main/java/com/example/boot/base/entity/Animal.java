package com.example.boot.base.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xmm
 * @since 2020/3/28
 */
@Data
public class Animal implements Serializable {
    String name;
    String age;
    String area;
    String master;
    String petName;
    String petType;
}
