package com.example.boot.base.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xmm
 * @since 2020/4/2
 */
@Slf4j
@RestController
@RequestMapping("/time")
@AllArgsConstructor
public class TimeController {


}