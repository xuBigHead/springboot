package com.example.boot.base.config;

import com.example.boot.base.entity.diy.Properties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author xmm
 * @since 2020/3/10
 */
@Data
@Slf4j
@PropertySource(value = "classpath:diy.properties")
@Configuration
public class PropertiesConfiguration {
    @Value("${name}")
    public String name;

    @Bean
    public Properties getProperties(){
        Properties properties = new Properties();
        properties.setName(name);
        log.info("properties: {}", properties);
        return properties;
    }
}
