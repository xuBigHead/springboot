package com.example.boot.base.controller;

import com.example.boot.base.entity.Animal;
import com.example.common.result.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author xmm
 * @since 2020/3/28
 */
@Slf4j
@RestController
@RequestMapping("/request")
@AllArgsConstructor
public class RequestController {
    @PostMapping("/get-request-body")
    public R getRequestBody(@RequestBody Animal animal){
        //Human human = Func.copy(animal, Human.class);
        //System.err.println(human);

        return R.data(null);
    }
}
