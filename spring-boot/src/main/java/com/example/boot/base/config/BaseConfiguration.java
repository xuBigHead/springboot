package com.example.boot.base.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 注解@Configuration表明这是一个配置Spring的配置类，相当于一个xml文件
 * @author xmm
 * @since 2020/3/5
 */
@Configuration
@ComponentScan(basePackages = "com.example.boot.base.component")
public class BaseConfiguration {

}
