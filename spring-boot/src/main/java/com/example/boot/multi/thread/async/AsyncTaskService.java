package com.example.boot.multi.thread.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

/**
 * @author xmm
 * @since 2020/4/20
 */
@Service
public class AsyncTaskService {
    @Async
    public void executeAsyncTaskWithoutReturn (int i) {
        System.out.println("线程" + Thread.currentThread().getName() + " 执行异步任务：" + i);
    }

    @Async
    public Future<String> executeAsyncTaskWithReturn (int i) {
        System.out.println("线程" + Thread.currentThread().getName() + " 执行异步任务：" + i);
        return new AsyncResult<>("success");
    }
}
