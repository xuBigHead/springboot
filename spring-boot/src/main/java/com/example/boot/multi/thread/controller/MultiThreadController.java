package com.example.boot.multi.thread.controller;

import com.example.boot.multi.thread.async.AsyncTaskService;
import com.example.boot.multi.thread.config.MultiThreadExecutor;
import com.example.boot.multi.thread.task.MultiThreadSerial;
import com.example.common.result.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

/**
 * @author mangmang.xu
 * @since 2020/4/20
 */
@Slf4j
@Component
@RestController
@RequestMapping("/thread")
@AllArgsConstructor
public class MultiThreadController {

    private ThreadPoolTaskExecutor taskExecutor;

    private AsyncTaskService asyncTaskService;

    @GetMapping("/test-multi-thread/{number}")
    public R getResultOfMultiThread(@PathVariable("number") Integer number) throws ExecutionException, InterruptedException {
        log.info("========request start========");
        if(1 == number) {
            asyncTaskService.executeAsyncTaskWithoutReturn(number);
            return R.success("success");
        } else if (2 == number) {
            Future<String> stringFuture = asyncTaskService.executeAsyncTaskWithReturn(number);
            return R.data(stringFuture.get());
        } else if (3 == number) {
            Future<Long> stringFuture = taskExecutor.submit(new MultiThreadSerial.Stats("thread 3" + number,1));
            return R.data(stringFuture.get());
        }
        return null;
    }
}
