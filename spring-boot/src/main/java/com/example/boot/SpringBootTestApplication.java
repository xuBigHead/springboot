package com.example.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;

/**
 * 注解@SpringBootApplication是SpringBoot项目的核心注解，主要目的是开启自动配置
 * 该注解由一下注解组成
 *   -@SpringBootConfiguration是SpringBoot项目的配置注解，也是一个组合注解
 *   -@EnableAutoConfiguration启用自动配置，该注解会使SpringBoot根据项目中依赖的jar包自动配置项目的配置项
 *      自动配置原理：
 *          SpringBoot在进行SpringApplication对象实例化时会加载META-INF/spring.factories文件，
 *          将该配置文件中的配置载入到Spring容器
 *      如果想要手动配置某一项，可以使用注解@SpringBootApplication的exclude属性手动指定
 *   -@ComponentScan默认扫描@SpringBootApplication所在类的同级目录以及它的子目录
 * @author xmm
 */
@SpringBootApplication(exclude = RedisAutoConfiguration.class)
public class SpringBootTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootTestApplication.class, args);
    }
}
