package com.example.easy.excel.task;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.example.common.exception.SpringBootException;
import com.example.easy.excel.entity.dto.UserExcelDTO;
import com.example.easy.excel.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author xmm
 */
@AllArgsConstructor
@Component
public class ExcelAsyncTask {
    UserMapper userMapper;
    @Async
    public void saveAsync(List<UserExcelDTO> list) {
        try {
            Thread.sleep(1000*60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //int size = list.size();
        //for (int i = 0; i < size; i++) {
        //
        //}
        List<UserExcelDTO> subList = list.subList(0, 500);
        subList.forEach(user -> user.setId(IdWorker.getId()));
        // mybatis_plus的saveBatch()效率比saveUserBatch()低
        System.err.println("异步任务开始");
        try {
            userMapper.saveUserBatch(subList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SpringBootException(500, "Excel表格错误");
        }
        System.err.println("异步任务结束");
    }
}
