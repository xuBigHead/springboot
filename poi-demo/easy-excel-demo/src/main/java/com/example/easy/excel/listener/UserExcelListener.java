package com.example.easy.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.example.common.exception.SpringBootException;
import com.example.easy.excel.entity.dto.UserExcelDTO;
import com.example.easy.excel.service.ExcelService;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xmm
 */
@AllArgsConstructor
public class UserExcelListener extends AnalysisEventListener<UserExcelDTO> {
    ExcelService excelService;
    private int operation = 0;
    /**
     * 经测试最佳批量处理数据量为500
     */
    private static final int MAX_ROW = 10000;
    private static final int BATCH_COUNT = 500;
    List<UserExcelDTO> list = new ArrayList<>(500);

    public UserExcelListener(ExcelService excelService,int operation) {
        this.excelService = excelService;
        this.operation = operation;
    }
    @Override
    public void invoke(UserExcelDTO data, AnalysisContext context) {
        Integer rowIndex = context.readRowHolder().getRowIndex();
        list.add(data);
        list.forEach(member -> member.setId(IdWorker.getId()));
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (list.size() >= BATCH_COUNT) {
            System.err.println(rowIndex);
            if(rowIndex > MAX_ROW){
                throw new SpringBootException(500, "Excel数据量过大");
            }
            if(operation == 0){
                saveData();
            } else {
                operation();
            }
            // 存储完成清理 list
            list.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        Integer rowIndex = analysisContext.readRowHolder().getRowIndex();
        if(rowIndex > MAX_ROW){
            throw new SpringBootException(500, "Excel数据量不要超过10000条");
        }
        if(operation == 0){
            saveData();
        } else {
            operation();
        }
    }

    private void saveData() {
        excelService.save(list);
    }

    private void operation() {
        excelService.operation(list);
    }
}
