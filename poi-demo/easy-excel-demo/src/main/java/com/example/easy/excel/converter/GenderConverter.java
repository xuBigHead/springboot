package com.example.easy.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * @author xmm
 */
public class GenderConverter implements Converter<Integer> {
    public static final String MALE = "男";
    public static final String FEMALE = "女";
    @Override
    public Class supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Integer convertToJavaData(CellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String stringValue = cellData.getStringValue();
        if (MALE.equals(stringValue)){
            return 0;
        }else if (FEMALE.equals(stringValue)){
            return 1;
        }else {
            return null;
        }
    }

    @Override
    public CellData convertToExcelData(Integer value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        CellData cellData = new CellData(supportExcelTypeKey());
        if (value == 0){
            cellData.setStringValue(MALE);
        }else if (value == 1){
            cellData.setStringValue(FEMALE);
        }else {
            cellData.setStringValue(null);
        }
        return cellData;
    }
}
