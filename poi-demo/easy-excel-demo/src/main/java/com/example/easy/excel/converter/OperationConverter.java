package com.example.easy.excel.converter;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;


/**
 * @author xmm
 */
public class OperationConverter implements Converter<Integer> {
    public static final String SAVE_OR_UPDATE = "插入或更新";
    public static final String DELETE = "删除";
    @Override
    public Class supportJavaTypeKey() {
        return Integer.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return CellDataTypeEnum.STRING;
    }

    @Override
    public Integer convertToJavaData(CellData cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        String stringValue = cellData.getStringValue();
        if (SAVE_OR_UPDATE.equals(stringValue)){
            return 0;
        }else if (DELETE.equals(stringValue)){
            return 1;
        }else {
            return null;
        }
    }

    @Override
    public CellData convertToExcelData(Integer value, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) throws Exception {
        CellData cellData = new CellData(supportExcelTypeKey());
        if (value == 0){
            cellData.setStringValue(SAVE_OR_UPDATE);
        }else if(value == 1){
            cellData.setStringValue(DELETE);
        }else {
            cellData.setStringValue(null);
        }
        return cellData;
    }
}
