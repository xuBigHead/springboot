package com.example.easy.excel.handler;

import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.write.handler.CellWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;

import java.util.List;

/**
 * @author xmm
 */
public class CustomCellWriteHandler implements CellWriteHandler {

    @Override
    public void beforeCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row,
                                 Head head, Integer columnIndex, Integer relativeRowIndex, Boolean isHead) {
        if(isHead){
            Sheet sheet = writeSheetHolder.getSheet();
            sheet.removeRow(row);
        }
    }

    @Override
    public void afterCellCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Cell cell,
                                Head head, Integer relativeRowIndex, Boolean isHead) {

    }

    @Override
    public void afterCellDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder,
                                 List<CellData> cellDataList, Cell cell, Head head, Integer relativeRowIndex, Boolean isHead) {

        if(!isHead){
            String s = "";
            CellData cellData = cellDataList.get(0);
            if(cellData != null) {
                s = cellData.getStringValue();
            }
            String prefix = "采贝";
            if(StringUtils.isNotEmpty(s) && StringUtils.startsWith(s, prefix)){
                Workbook workbook = writeSheetHolder.getSheet().getWorkbook();
                CellStyle cellStyle = workbook.createCellStyle();
                Font cellFont = workbook.createFont();
                cellFont.setColor(IndexedColors.RED.getIndex());
                cellStyle.setFont(cellFont);
                cell.setCellStyle(cellStyle);
            }
        }
    }
}
