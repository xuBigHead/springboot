package com.example.easy.excel.entity.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.example.easy.excel.converter.GenderConverter;
import com.example.easy.excel.converter.OperationConverter;
import lombok.Data;

import java.util.Date;

/**
 * @author xmm
 */
@Data
@TableName("springboot_user")
public class UserExcelDTO {

    @ExcelIgnore
    @JsonIgnore
    private Long id;

    @ExcelProperty(value = "姓名(必填)",index = 0)
    private String realName;

    @ExcelProperty(value = "手机号码(必填)",index = 1)
    private String phone;

    @ExcelProperty(value = "分组(必填)",index = 2)
    private String dept;

    @ExcelProperty(value = "操作（必填）",index = 3,converter = OperationConverter.class)
    private Integer operation;

    @ExcelProperty(value = "工号",index = 4)
    private String jobNumber;

    @ExcelProperty(value = "性别",index = 5,converter = GenderConverter.class)
    private Integer sex;

    @ExcelProperty(value = "职位",index = 6)
    private String jobTitle;

    @ExcelProperty(value = "邮箱",index = 7)
    private String email;

    @ExcelProperty(value = "生日",index = 8)
    @DateTimeFormat("yyyy/MM/dd")
    private Date birthday;

    @ExcelProperty(value = "备注",index = 9)
    private String remark;
}
