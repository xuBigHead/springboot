package com.example.easy.excel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.easy.excel.entity.dto.UserExcelDTO;
import com.example.easy.excel.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xmm
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 批量添加用户
     * @param list 用户集合
     * @return 添加情况
     */
    int saveUserBatch(@Param("users") List<UserExcelDTO> list);
}
