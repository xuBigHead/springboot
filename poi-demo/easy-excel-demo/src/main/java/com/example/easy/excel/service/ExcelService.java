package com.example.easy.excel.service;

import com.example.easy.excel.entity.dto.UserExcelDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author xmm
 */
public interface ExcelService {
    /**
     * 导入excel
     * @param file excel文件
     * @return 数据集合
     */
    List<UserExcelDTO> importExcel(MultipartFile file);

    /**
     * 异步导入excel
     * @param file excel文件
     * @return 数据集合
     */
    String importExcelAsync(MultipartFile file);

    /**
     * 添加数据到数据库
     * @param list 数据集合
     * @return 添加结果
     */
    boolean save(List<UserExcelDTO> list);

    /**
     * 导入excel操作
     * @param list 数据集合
     * @return 操作结果
     */
    boolean operation(List<UserExcelDTO> list);


}
