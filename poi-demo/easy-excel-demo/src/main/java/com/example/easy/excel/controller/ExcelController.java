package com.example.easy.excel.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mangmang.xu
 * @since 2020/4/23
 */
@Slf4j
@RestController
@RequestMapping("/excel")
public class ExcelController {
}
