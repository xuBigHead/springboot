package com.example.easy.excel.wrapper;

import com.example.easy.excel.entity.dto.UserExcelDTO;
import com.example.easy.excel.entity.User;
import org.springframework.beans.BeanUtils;

public class UserWrapper {
    public static UserWrapper build() {
        return new UserWrapper();
    }
    public User entity(UserExcelDTO userExcelDTO) {
        User user = new User();
        BeanUtils.copyProperties(userExcelDTO, user);
        //String sex = userExcelDTO.getSex();
        //if(StringUtils.isNotEmpty(sex)){
        //    if(ExcelConstant.MAN.equals(sex)){
        //        user.setSex(1);
        //    }else if(ExcelConstant.WOMEN.equals(sex)) {
        //        user.setSex(0);
        //    }else {
        //        System.err.println("请填写正确性别");
        //        //throw new Exception();
        //    }
        //}
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //Date date = null;
        //try {
        //    date =  simpleDateFormat.parse(userExcelDTO.getBirthday());
        //} catch (ParseException e) {
        //    e.printStackTrace();
        //}
        user.setBirthday(userExcelDTO.getBirthday());
        return user;
    }
}
