package com.example.easy.excel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author xmm
 */
@Data
public class DailyCost {
    @ExcelProperty(value = "日期")
    private String date;
    @ExcelProperty(value = "描述")
    private String description;
    @ExcelProperty(value = "价格")
    private BigDecimal price;
}
