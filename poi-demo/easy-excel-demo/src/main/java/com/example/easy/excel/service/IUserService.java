package com.example.easy.excel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.easy.excel.entity.User;

/**
 * @author xmm
 */
public interface IUserService extends IService<User> {

}
