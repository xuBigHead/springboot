package com.example.easy.excel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.easy.excel.mapper.UserMapper;
import com.example.easy.excel.service.IUserService;
import com.example.easy.excel.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author xmm
 */
@Service
@AllArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
