package com.example.mybatis.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author xmm
 * @since 2020/1/8
 */
@Data
public class User {
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String realName;

    private Integer sex;

    private String phone;

    private String dept;

    private String jobNumber;

    private String jobTitle;

    private String email;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    private String remark;
}
