package com.example.mybatis.controller;

import com.example.common.result.R;
import com.example.mybatis.entity.User;
import com.example.mybatis.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author xmm
 * @since 2020/1/8
 */
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    UserService userService;

    @GetMapping("/list")
    public R<List<User>> list(){
        return userService.list();
    }

    @GetMapping("/list/results")
    public R<List<User>> listWithResults(){
        return userService.listWithResults();
    }

    @GetMapping("/list/results/provider")
    public R<List<User>> listWithResultsBySqlProvider(){
        return userService.listWithResultsBySqlProvider();
    }
}
