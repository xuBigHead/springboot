package com.example.mybatis.mapper;

import com.example.mybatis.entity.User;
import com.example.mybatis.provider.UserSqlProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author xmm
 * @since 2020/1/8
 */
@Mapper
public interface UserMapper {
    @Select("select * from springboot_user where id > 1204711282827433220")
    List<User> list();

    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "realName", column = "real_name"),
            @Result(property = "phone", column = "phone"),
            @Result(property = "sex", column = "sex")
    })
    @Select("select `id`,`real_name` from springboot_user where id = 1204711282827433220")
    List<User> listWithResults();

    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "realName", column = "real_name")
    })
    @SelectProvider(type = UserSqlProvider.class, method = "getUserList")
    List<User> listWithResultsBySqlProvider();
    //@Insert("insert into springboot (`name`) values #{}")
    //int save(User user);
}
