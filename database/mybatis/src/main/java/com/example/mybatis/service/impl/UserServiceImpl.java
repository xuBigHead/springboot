package com.example.mybatis.service.impl;

import com.example.common.result.R;
import com.example.mybatis.entity.User;
import com.example.mybatis.mapper.UserMapper;
import com.example.mybatis.mapper.UserXMLMapper;
import com.example.mybatis.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xmm
 * @since 2020/1/8
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    UserMapper userMapper;
    UserXMLMapper userXMLMapper;
    @Override
    public R<List<User>> list() {
        List<User> userList = userXMLMapper.list();
        return R.data(userMapper.list());
    }

    @Override
    public R<List<User>> listWithResults() {
        List<User> userList = userMapper.listWithResults();
        return R.data(userList);
    }

    @Override
    public R<List<User>> listWithResultsBySqlProvider() {
        return R.data(userMapper.listWithResultsBySqlProvider());
    }
}
