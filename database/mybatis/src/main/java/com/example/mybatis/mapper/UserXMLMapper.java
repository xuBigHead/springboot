package com.example.mybatis.mapper;

import com.example.mybatis.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author xmm
 * @since 2020/1/9
 */
@Mapper
public interface UserXMLMapper {
    List<User> list();
}
