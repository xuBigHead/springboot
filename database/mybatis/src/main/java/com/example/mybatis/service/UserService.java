package com.example.mybatis.service;

import com.example.common.result.R;
import com.example.mybatis.entity.User;

import java.util.List;

/**
 * @author xmm
 * @since 2020/1/8
 */
public interface UserService {
    /**
     * return user list
     * @return list
     */
    R<List<User>> list();

    /**
     * return user list with results
     * @return list
     */
    R<List<User>> listWithResults();

    /**
     * return user list with results by sql provider
     * @return list
     */
    R<List<User>> listWithResultsBySqlProvider();
}
