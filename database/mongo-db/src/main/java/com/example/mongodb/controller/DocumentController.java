package com.example.mongodb.controller;

import com.example.mongodb.entity.DocumentBean;
import com.example.mongodb.service.IDocumentService;
import com.example.common.result.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xmm
 * @since 2020/3/26
 */
@RestController
@RequestMapping("/doc")
@AllArgsConstructor
public class DocumentController {
    IDocumentService documentService;
    @PostMapping("/save")
    public R save(@RequestBody DocumentBean documentBean){
        return documentService.save(documentBean);
    }

    @PostMapping("/delete")
    public R delete(@RequestBody DocumentBean documentBean) {
        return documentService.delete(documentBean);
    }

    @PostMapping("/update")
    public R update(@RequestBody DocumentBean documentBean) {
        return documentService.update(documentBean);
    }

    @GetMapping("/detail")
    public R detail(@RequestParam(value = "docId") Long docId){
        return documentService.getDocDetail(docId);
    }

    @GetMapping("/list")
    public R<List<DocumentBean>> list(@RequestParam(value = "title") String title){
        return documentService.getDocList(title);
    }
}
