package com.example.mongodb.repository;

import com.example.mongodb.entity.DocumentBean;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author xmm
 * @since 2020/3/26
 */
@Repository
public interface DocumentRepository extends MongoRepository<DocumentBean, Long> {
}
