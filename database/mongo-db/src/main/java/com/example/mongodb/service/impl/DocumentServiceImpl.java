package com.example.mongodb.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.example.common.result.R;
import com.example.common.utils.BeanBuilderUtil;
import com.example.mongodb.entity.DocumentBean;
import com.example.mongodb.repository.DocumentRepository;
import com.example.mongodb.service.IDocumentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author xmm
 * @since 2020/3/26
 */
@Service
@AllArgsConstructor
public class DocumentServiceImpl implements IDocumentService {
    DocumentRepository documentRepository;
    MongoTemplate mongoTemplate;

    @Override
    public R save(DocumentBean document) {
        long id = IdWorker.getId();
        document.setId(id);
        document.setCreateTime(new Date());
        document.setUpdateTime(new Date());
        document.setIsDeleted(1);
        DocumentBean documentBeanFromMongo = documentRepository.save(document);
        return R.data(documentBeanFromMongo);
    }

    @Override
    public R delete(DocumentBean documentBean) {
        documentRepository.deleteById(documentBean.getId());
        return R.success("success");
    }

    @Override
    public R update(DocumentBean documentBean) {

        return null;
    }

    @Override
    public R getDocDetail(Long docId) {
        Optional<DocumentBean> optional = documentRepository.findById(docId);
        DocumentBean document = optional.orElse(null);
        return R.data(document);
    }

    @Override
    public R<List<DocumentBean>> getDocList(String title) {
        DocumentBean documentBean = BeanBuilderUtil.of(DocumentBean::new).with(DocumentBean::setTitle, title).build();
        Example<DocumentBean> documentBeanExample = Example.of(documentBean,ExampleMatcher.matchingAll());
        List<DocumentBean> documentBeans = documentRepository.findAll(documentBeanExample);
        return R.data(documentBeans);
    }
}
