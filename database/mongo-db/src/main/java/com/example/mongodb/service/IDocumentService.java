package com.example.mongodb.service;

import com.example.common.result.R;
import com.example.mongodb.entity.DocumentBean;

import java.util.List;

/**
 * @author xmm
 * @since 2020/3/26
 */
public interface IDocumentService {
    /**
     * insert new doc
     *
     * @param document doc
     * @return uniform return object
     */
    R save(DocumentBean document);

    /**
     * delete doc from mongo
     *
     * @param documentBean doc
     * @return uniform return object
     */
    R delete(DocumentBean documentBean);

    /**
     * update doc from mongo
     *
     * @param documentBean doc
     * @return uniform return object
     */
    R update(DocumentBean documentBean);

    /**
     * get detail info of doc from mongo
     *
     * @param docId doc id
     * @return doc info
     */
    R getDocDetail(Long docId);

    /**
     * get doc list
     *
     * @param title doc title
     * @return doc list
     */
    R<List<DocumentBean>> getDocList(String title);
}
