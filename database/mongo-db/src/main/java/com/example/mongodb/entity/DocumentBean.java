package com.example.mongodb.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xmm
 * @since 2020/3/26
 */
@Data
@Document("document")
public class DocumentBean {
    Long id;
    /**
     * 文档标题
     */
    String title;
    /**
     * 文档标题
     */
    String author;
    /**
     * 文档章节标题
     */
    List<String> chapters;
    /**
     * 文档章节内容
     */
    Map<String,String> content;
    /**
     * 文档章节字数
     */
    Map<String,Long> wordCount;
    /**
     * 文档章节图片URL地址
     */
    Map<String,List<String>> imageUrls;
    /**
     * 文档价格
     */
    BigDecimal price;
    /**
     * 文档创建时间
     */
    Date createTime;
    /**
     * 文档最后修改时间
     */
    Date updateTime;
    /**
     * 文档标题
     */
    Integer isDeleted;
}
