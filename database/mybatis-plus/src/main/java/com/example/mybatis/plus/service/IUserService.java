package com.example.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.entity.User;

/**
 * @author xmm
 * @since 2020/1/16
 */
public interface IUserService extends IService<User> {
}
