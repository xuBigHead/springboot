package com.example.mybatis.plus.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.common.entity.User;
import com.example.common.result.R;
import com.example.mybatis.plus.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.sql.Wrapper;
import java.util.function.Function;

/**
 * @author xmm
 * @since 2020/3/10
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {
    IUserService userService;
    @RequestMapping("/list")
    public R list() {
        return R.data(userService.list());
    }

    @PostMapping("/save")
    public R save(@RequestBody User user) {
        return R.status(userService.save(user));
    }

    @GetMapping("/get-username")
    public R getUsername(@RequestParam("userId") Long userId){
        Object username = userService.getObj(
                Wrappers.<User>lambdaQuery()
                        .eq(User::getId, userId).select(User::getRealName,User::getBirthday),
                Function.identity());
        return R.data(username);
    }

}
