package com.example.elk.repository;

import com.example.elk.entity.MaterialRecord;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author xmm
 * @since 2020/3/29
 */
public interface IMaterialRecordRepository extends ElasticsearchRepository<MaterialRecord,Long> {
}