package com.example.elk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xmm
 * @since 2020/3/29
 */
@SpringBootApplication
public class ELKApplication {
    public static void main(String[] args) {
        SpringApplication.run(ELKApplication.class, args);
    }
}

