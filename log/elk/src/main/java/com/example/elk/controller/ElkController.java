package com.example.elk.controller;

import com.example.common.result.R;
import com.example.elk.entity.MaterialRecord;
import com.example.elk.repository.IMaterialRecordRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xmm
 * @since 2020/3/29
 */
@Slf4j
@RestController
@RequestMapping("/elk")
@AllArgsConstructor
public class ElkController {
    //ElasticsearchTemplate elasticsearchTemplate;
    IMaterialRecordRepository materialRecordRepository;
    RestHighLevelClient highClient;
    RequestOptions requestOptions;

    @GetMapping("/create-index")
    public R createIndex(){
        //elasticsearchTemplate.createIndex(MaterialRecord.class);
        return null;
    }

    @GetMapping("/delete-index")
    public R deleteIndex(){
        //elasticsearchTemplate.deleteIndex(MaterialRecord.class);
        return null;
    }

    @PostMapping("/submit")
    public R submit(@RequestBody MaterialRecord materialRecord) {
        log.info("request param is [{}]",materialRecord);
        materialRecordRepository.save(materialRecord);
        return null;
    }

    @GetMapping("/main")
    public R main(){
        try {
            // 创建request。
            Map<String, Object> jsonMap = new HashMap<>();
            // field_01、field_02为字段名，value_01、value_02为对应的值。
            jsonMap.put("{field_01}", "{value_01}");
            jsonMap.put("{field_02}", "{value_02}");
            //index_name为索引名称；type_name为类型名称；doc_id为文档的id。
            IndexRequest indexRequest = new IndexRequest("{index_name}", "{type_name}", "{doc_id}").source(jsonMap);

            // 同步执行，并使用自定义RequestOptions（COMMON_OPTIONS）。
            IndexResponse indexResponse = highClient.index(indexRequest, requestOptions);

            long version = indexResponse.getVersion();

            System.out.println("Index document successfully! " + version);
            //index_name为索引名称；type_name为类型名称；doc_id为文档的id。与以上创建索引的名称和id相同。
            DeleteRequest request = new DeleteRequest("{index_name}", "{type_name}", "{doc_id}");
            DeleteResponse deleteResponse = highClient.delete(request, requestOptions);

            System.out.println("Delete document successfully! \n" + deleteResponse.toString() + "\n" + deleteResponse.status());

            highClient.close();

        } catch (IOException ioException) {
            // 异常处理。
        }
        return null;
    }
}
