package com.example.elk.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author xmm
 * @since 2020/3/27
 */
@Data
@Document(indexName = "material-record",type = "docs", shards = 1, replicas = 0)
public class MaterialRecord {

	/**
	 * 主键ID
	 */
	@Id
	@JsonSerialize(using = ToStringSerializer.class)
	@Field(type = FieldType.Keyword)
	Long id;

	/**
	 * 素材ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@NotNull
	@Field(type = FieldType.Keyword)
	Long materialId;

	/**
	 * 任务ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@NotNull
	@Field(type = FieldType.Keyword)
	Long jobId;

	/**
	 * 用户ID
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@Field(type = FieldType.Keyword)
	Long userId;

	/**
	 * 开始学习时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Field(type = FieldType.Keyword)
	Date startTime;

	/**
	 * 结束学习时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Field(type = FieldType.Keyword)
	Date endTime;

	/**
	 * 学习时长
	 */
	@Field(type = FieldType.Keyword)
	Integer duration;

	/**
	 * 学习次数
	 */
	@Field(type = FieldType.Keyword)
	Integer learningCount;

	/**
	 * 学习状态
	 */
	@Field(type = FieldType.Keyword)
	Integer status;

	/**
	 * 创建时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Field(type = FieldType.Keyword)
	Date createTime;

	/**
	 * 修改时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Field(type = FieldType.Keyword)
	Date updateTime;
}
