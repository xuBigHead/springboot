package com.example.logback.controller;

import com.example.common.result.R;
import com.example.logback.entity.LogEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author xmm
 * @since 2020/1/1
 */
@Slf4j
@RestController
@RequestMapping("/com/example/logback")
public class LogController {
    @GetMapping("/{num}")
    public void logGet(@PathVariable("num") Integer num){
        log.warn("warn:传入参数值可能为0");
        try {
            log.info("info:传入参数num值为[{}]",num);
            log.debug("debug:传入参数num值为[{}]",num);
            int i = 10/num;
        } catch (Exception e) {
            log.error("error:传入参数值为0");
        }
    }

    @PostMapping("/entity")
    public R logPost(@RequestBody LogEntity entity) {
        log.info("info:传入的参数为[{}]", entity);
        Integer number = entity.getNumber();
        try {
            log.warn("warn:被除数可能为[{}]",number);
            int i = 10/ number;
        } catch (Exception e) {
            log.error("error:算术运算异常，被除数为[{}]",number);
        }
        return R.data(entity);
    }
}
