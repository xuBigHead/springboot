package com.example.logback.entity;

import lombok.Data;

/**
 * @author xmm
 * @since 2020/1/14
 */
@Data
public class LogEntity {
    String name;
    String level;
    Integer number;
}
