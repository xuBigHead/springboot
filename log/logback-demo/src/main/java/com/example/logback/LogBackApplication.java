package com.example.logback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xmm
 */
@SpringBootApplication
public class LogBackApplication {
    public static void main(String[] args) {
        try {
            SpringApplication.run(LogBackApplication.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
