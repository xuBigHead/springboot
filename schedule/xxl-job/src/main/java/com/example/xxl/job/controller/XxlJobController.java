package com.example.xxl.job.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mangmang.xu
 * @since 2020/4/15
 */
@Slf4j
@RestController
@RequestMapping("/xxl-job")
public class XxlJobController {

}
