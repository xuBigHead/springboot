package com.example.quartz.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author xmm
 * @since 2020/1/7
 */
@Component
@Configurable
@EnableScheduling
@Slf4j
public class ScheduleTask {
    /**
     * cron 规则 [秒（0~59）] [分钟（0~59）] [小时（0~23）] [天（0~31）] [月（0~11）]
     * [星期（1~7 1=SUN 或 SUN，MON，TUE，WED，THU，FRI，SAT）] [年份（1970－2099）]
     */
    @Scheduled(cron = "*/2 * * * * *")
    public void printTime(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(new Date());
        log.info("现在是北京时间[{}]", format);
    }

    @Scheduled(cron = "*/10 * * * * *")
    public void printMultiThread(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(new Date());
        log.info("开始执行定时任务，当前时间[{}]",format);
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("执行完毕，当前时间[{}]",format);
    }
}
