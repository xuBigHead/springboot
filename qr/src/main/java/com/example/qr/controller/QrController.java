package com.example.qr.controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * 二维码相关controller类
 * 322233
 * @author xmm
 */
@Slf4j
@RestController
@RequestMapping("/qr")
public class QrController {
    @GetMapping("/get/code")
    public void getQrCode(){
        log.info("============获取二维码==========");
        String text = "www.baidu.com";
        int width = 350;
        int height = 350;
        String filePath = "C:/Users/xmm/Desktop/qrCode.png";
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = null;
        try {
            bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
            Path path = FileSystems.getDefault().getPath(filePath);
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
        } catch (IOException | WriterException e) {
            e.printStackTrace();
        }
    }
}



