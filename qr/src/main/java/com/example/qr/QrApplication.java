package com.example.qr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author xmm
 * @since 2020/1/7
 */
@SpringBootApplication
@EnableAsync
public class QrApplication {
    public static void main(String[] args) {
        SpringApplication.run(QrApplication.class, args);
    }
}
