package com.example.common.result;

import java.io.Serializable;

/**
 * @author xmm
 */
public interface IResultCode extends Serializable {
    /**
     * 获取消息
     */
    String getMessage();

    /**
     * 获取状态码
     */
    int getCode();
}
