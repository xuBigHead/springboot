package com.example.common.entity.collection;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xmm
 * @since 2020/1/11
 */
@Data
public class NumList {
    private  List<Integer> numList = new ArrayList<>(10);
    {
        numList.add(0);
        numList.add(1);
        numList.add(2);
        numList.add(3);
        numList.add(4);
        numList.add(5);
        numList.add(6);
        numList.add(7);
        numList.add(8);
        numList.add(9);
    }
}
