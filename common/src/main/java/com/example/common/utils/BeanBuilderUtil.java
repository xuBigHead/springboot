package com.example.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * 通用的 BeanBuilderUtil 模式构建器
 *
 * @author: xmm
 * @since 2020/3/17
 */
public class BeanBuilderUtil<T> {
	private final Supplier<T> instance;
	private List<Consumer<T>> modifiers = new ArrayList<>();

	private BeanBuilderUtil(Supplier<T> instance) {
		this.instance = instance;
	}

	public static <T> BeanBuilderUtil<T> of(Supplier<T> instance) {
		return new BeanBuilderUtil<>(instance);
	}

	public <P1> BeanBuilderUtil<T> with(Consumer1<T, P1> consumer, P1 p1) {
		Consumer<T> c = instance -> consumer.accept(instance, p1);
		modifiers.add(c);
		return this;
	}

	public <P1, P2> BeanBuilderUtil<T> with(Consumer2<T, P1, P2> consumer, P1 p1, P2 p2) {
		Consumer<T> c = instance -> consumer.accept(instance, p1, p2);
		modifiers.add(c);
		return this;
	}

	public <P1, P2, P3> BeanBuilderUtil<T> with(Consumer3<T, P1, P2, P3> consumer, P1 p1, P2 p2, P3 p3) {
		Consumer<T> c = instance -> consumer.accept(instance, p1, p2, p3);
		modifiers.add(c);
		return this;
	}

	public T build() {
		T value = instance.get();
		modifiers.forEach(modifier -> modifier.accept(value));
		modifiers.clear();
		return value;
	}

	@FunctionalInterface
	public interface Consumer1<T, P1> {
		/**
		 * accept
		 *
		 * @param t  instance
		 * @param p1 param1
		 */
		void accept(T t, P1 p1);
	}

	@FunctionalInterface
	public interface Consumer2<T, P1, P2> {
		/**
		 * accept
		 *
		 * @param t  instance
		 * @param p1 param1
		 * @param p2 param2
		 */
		void accept(T t, P1 p1, P2 p2);
	}

	@FunctionalInterface
	public interface Consumer3<T, P1, P2, P3> {
		/**
		 * accept
		 *
		 * @param t  instance
		 * @param p1 param1
		 * @param p2 param2
		 * @param p3 param3
		 */
		void accept(T t, P1 p1, P2 p2, P3 p3);
	}
}

