package com.example.common.utils;

import com.example.common.result.R;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

/**
 * @author xmm
 * @since 2020/1/12
 */
@Slf4j
public class ControllerUtil<T> {
    public static <T> ControllerUtil<T> build() {
        return new ControllerUtil<>();
    }
    public R run(T t, String method) {
        String interfaceName = t.getClass().getInterfaces()[0].getName();
        log.info("=====调用[{}.{}]方法=====",interfaceName,method);
        try {
            Method clazzMethod =  t.getClass().getDeclaredMethod(method);
            clazzMethod.invoke(t);
            log.info("=====[{}.{}]方法执行完毕=====",interfaceName,method);
        } catch (Exception e) {
            log.error("=====[{}].[{}]方法执行异常=====",interfaceName,method,e);
            return R.fail("=====" + method + "方法执行异常=====");
        }
        return R.success("=====" + method + "方法执行完毕=====");
    }
}
