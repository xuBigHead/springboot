package com.example.common.exception;

import com.example.common.result.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author xmm
 */
@Data
@ApiModel("自定义异常")
@EqualsAndHashCode(callSuper = true)
public class SpringBootException extends RuntimeException{
    /**
     * 接受状态码和消息
     */
    @ApiModelProperty(value = "状态码")
    private Integer code;
    public SpringBootException(Integer code, String message){
        super(message);
        this.code = code;
    }
    public SpringBootException(ResultCode resultCode){
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
    }
    @Override
    public String toString() {
        return "SpringBootException{" +
                "message=" + this.getMessage() +
                ", code=" + code +
                '}';
    }
}
