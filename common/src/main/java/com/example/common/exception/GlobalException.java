package com.example.common.exception;

import com.example.common.result.R;
import com.example.common.utils.ExceptionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author xmm
 */
@ControllerAdvice
@Slf4j
public class GlobalException {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R error(Exception e){
        // 异常处理：一般会打印日志，并通过邮件或短信通知运维/开发人员
        System.err.println("Exception:" + e);
        log.error(ExceptionUtil.getMessage(e));
        return R.fail(e.getMessage());
    }
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public R errorArithmetic(ArithmeticException e){
        System.err.println("ArithmeticException:" + e);
        log.error(ExceptionUtil.getMessage(e));
        return R.fail(e.getMessage());
    }
    @ExceptionHandler(SpringBootException.class)
    @ResponseBody
    public R errorArithmetic(SpringBootException e){
        log.error(ExceptionUtil.getMessage(e));
        return R.fail(e.getMessage());
    }
}
