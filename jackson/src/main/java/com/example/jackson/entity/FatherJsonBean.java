package com.example.jackson.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
// JsonIgnoreProperties注解可以批量忽略该类中的属性和未知属性
@JsonIgnoreProperties(ignoreUnknown=true,value = {"confirmPassword" , "realPassword"})
public class FatherJsonBean {
    private String name;

    private Integer age;

    // JsonProperty注解可以以别名来映射对应的属性
    @JsonProperty("sex")
    private String gender;

    private String phoneNumber;

    private String address;
    @JsonIgnore
    private String account;
    // JsonIgnore注解可以忽略标注的属性
    private String password;

    private String confirmPassword;

    private String realPassword;


    public static FatherJsonBean getInstance(){
        FatherJsonBean fatherJsonBean = new FatherJsonBean();
        fatherJsonBean.setName("Tom");
        fatherJsonBean.setAge(18);
        fatherJsonBean.setGender("man");
        fatherJsonBean.setPhoneNumber("15755507237");
        fatherJsonBean.setAddress("China");
        fatherJsonBean.setAccount("1390589756");
        fatherJsonBean.setPassword("123");
        fatherJsonBean.setConfirmPassword("1234");
        fatherJsonBean.setRealPassword("12345");
        return fatherJsonBean;
    }
}
