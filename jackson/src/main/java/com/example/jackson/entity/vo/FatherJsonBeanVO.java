package com.example.jackson.entity.vo;

import com.example.jackson.entity.FatherJsonBean;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author xmm
 */
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FatherJsonBeanVO extends FatherJsonBean {
    @JsonProperty("sexVO")
    private String gender;
}
