package com.example.jackson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author xmm
 */
@SpringBootApplication
@EnableAsync
public class JacksonApplication {
    public static void main(String[] args) {
        SpringApplication.run(JacksonApplication.class, args);
    }
}
